name = epiqo
description = Default layout for epiqo distributions.

regions[navigation] = Navigation
regions[preface] = Preface
regions[content] = Content
regions[postscript] = Postscript
regions[footer] = Footer
regions[sidebar_first] = First Sidebar
regions[sidebar_second] = Second Sidebar

stylesheets[all][] = css/epiqo.css
scripts[] = js/jquery.offcanvas.js
scripts[] = js/epiqo.js

preview = preview.png
