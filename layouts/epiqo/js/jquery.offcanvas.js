;(function ($, window, document, undefined) {
  "use strict";

  // Plugin name for use in
  var pluginName = "offcanvas";

  /**
   * Plugin constructor.
   */
  var OffCanvas = function (element, options) {
    this.name = pluginName;

    // Store a reference to the loading element.
    this.$element = $(element);
    $(this.$element).data = (pluginName, this);

    // Allow users to override the default configuration.
    this.options = options;

    // It is also possible to override the configuration on a per-element basis
    // via the 'data-offcanvas-options' attribute.
    this.metadata = this.$element.data('offcanvas-options');

    // Merge the configuration together. We obtain configuration from three
    // different sources (lowest precedences to highest precedence):
    // - Defaults (@see OffCanvas.prototype.defaults)
    // - Options (User defined overrides)
    // - Metadata (Per-element overrides derived from the off-canvas-options
    //   attributes.
    this.config = $.extend({}, this.defaults, this.options, this.metadata);

    this.init();
  }

  /**
   * Plugin prototype.
   */
  OffCanvas.prototype = {
    
    /**
     * Defaults.
     */
    defaults: {
      'body': $('<div class="off-canvas--body" />'),
      'panel': $('<div class="off-canvas--panel" />'),
      'parent': $('body'),
      'navigation': $('body'),
      'orientation': "left",
      'active': false,
      'trigger': $('<a href="#" class="off-canvas--trigger" />').click(function () {
        // Return false to skip the hash tag in the href attribute.
        return false;
      })
    },

    init: function () {
      // Check if we this panel should be active initially.
      if (this.config.active === true) {
        this.activate();
      }

      $(this.get('trigger')).click($.proxy(function () {
        this.toggle();
      }, this)).prependTo(this.get('navigation'));
    },

    destroy: function () {
      if (this.active === true) {
        this.deactivate();
      }
    },

    toggle: function () {
      if (this.active === true) {
        this.deactivate();
      }
      else {
        this.activate();
      }
    },

    activate: function () {
      this.get('parent').addClass('off-canvas--' + this.get('orientation'));

      // Wrap everything up so we can easily push it off the canvas.
      this.$body = this.get('parent').wrapInner(this.get('body')).contents().click($.proxy(function () {
        this.deactivate();
      }, this));

      // Clone the subject, so we can stick it into our off-canvas panel. Moving
      // it around is impractical because we would then have to move it back
      // depending on the screen size.
      this.$panel = this.get('panel').html(this.$element.contents().clone(true)).insertBefore(this.$body);

      // Mark it as active.
      this.active = true;
    },

    deactivate: function () {
      this.get('parent').removeClass('off-canvas--' + this.get('orientation'));

      // Delete the cloned panel and the body wrapper.
      this.$panel.remove();
      this.$body.contents().unwrap();

      // Mark it as inactive.
      this.active = false;
    },

    get: function (item) {
      if ($.isFunction(this.config[item])) {
        return this.config[item].apply(this);
      }
      return this.config[item];
    }
  }

  // Plugin entry point.
  $.fn.offcanvas = function (options) {
    return this.each(function () {
      // Make sure we don't execute the plugin twice for the same object.
      if (undefined == $(this).data(pluginName)) {
        new OffCanvas(this, options);
      }
    });
  };

})(jQuery, window, document);
